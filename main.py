from image import get_img, get_img_hash
from image_buffer import ImageBuffer
from network_connect import Network
from machine import lightsleep
from config import wifi_ssid, wifi_password


def main():
    epd = ImageBuffer()
    network = Network(wifi_ssid, wifi_password)
    byte_array_hash = None
    first_run = True
    print("Starting main function")

    epd.Clear()
    epd.delay_ms(2000)

    while True:
        print("Running loop")
        network.connect()
        print("Getting new byte_array hash")
        new_byte_array_hash = get_img_hash()

        if new_byte_array_hash != byte_array_hash:
            print("New image available")
            byte_array_hash = new_byte_array_hash

            print("Getting new byte_array")
            byte_array = get_img()
            network.disconnect()

            if first_run:
                epd.Clear()
                first_run = False
            else:
                epd.init()

            epd.apply_image(byte_array)

            epd.delay_ms(2000)
            epd.sleep()
        else:
            print("No new image available")

        network.disconnect()
        # Sleep for 5 minutes
        sleepTime = 5 * 60 * 1000
        lightsleep(sleepTime)


if __name__ == '__main__':
    main()
