from epaper import EPD_Landscape


class ImageBuffer(EPD_Landscape):
    def __init__(self):
        super().__init__()

    def apply_image(self, byte_array):
        # Reverse the width and height to match the display orientation
        height, width = self.width, self.height  # Get display dimensions

        # Ensure the byte_array size matches the buffer size
        if len(byte_array) == len(self.buffer):
            # self.Clear()  # Clear the display frame

            for y in range(height):
                for x in range(width):

                    array_index = (x + (y * width)) // 8
                    array_byte = byte_array[array_index]
                    bit_index = (x + (y * width)) % 8
                    bit = (array_byte & (1 << bit_index)) == (1 << bit_index)

                    c = 0xff if bit == 0 else 0x00

                    self.pixel(x, y, c)

            # Update the display with the new buffer
            self.display(self.buffer)
        else:
            print("Error: Byte array size does not match the display buffer size")
            print("Byte array size: ", len(byte_array))
            print("Display buffer size: ", len(self.buffer))
            print("Display dimensions: ", width, "x", height)
