import network
from time import sleep


class Network:
    def __init__(self, ssid, password):
        self.SSID = ssid
        self.PASSWORD = password
        self.IP_ADDR = ''

    def connect(self):
        print("Connecting to WiFi")
        # Connect to WLAN
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        wlan.connect(self.SSID, self.PASSWORD)
        while not wlan.isconnected():
            print('Waiting for connection...')
            sleep(1)
        print("Connected to WiFi")
        self.IP_ADDR = wlan.ifconfig()[0]

    def disconnect(self):
        wlan = network.WLAN(network.STA_IF)
        wlan.disconnect()
        wlan.active(False)
        wlan.deinit()
        wlan = None
        sleep(1)
        print("Disconnected from WiFi")


