import urequests
from config import paperdraw_server_domain


def get_page(url):
    response = urequests.get(url)

    if response.status_code == 200:
        response_data = response.content
        print("Page fetched successfully")
    else:
        print("Failed to fetch page")
        response_data = None

    response.close()
    return response_data


def get_img():
    # URL of the endpoint that returns the byte array
    url = f'{paperdraw_server_domain}/image_resized'

    # Make the GET request to fetch the byte array
    return get_page(url)


def get_img_hash():
    # URL of the endpoint that returns the hash of the byte array
    url = f'{paperdraw_server_domain}/image_hash'

    # Make the GET request to fetch the byte array
    return get_page(url)
